package multiplayertriviagameclient;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.BooleanControl;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

public class TriviaClient extends JFrame {

    CardLayout layout;
    JPanel all;
    final static String WELCOME = "WELCOME";
    final static String START = "START";
    final static String GAMESCREEN = "GAMESCREEN";

    JPanel welcome = new JPanel();
    JPanel start = new JPanel();
    JPanel gameScreen = new JPanel();

    HelloName[] helloNames = new HelloName[4];
    SoundBtn[] soundBtns = new SoundBtn[5];

    private JLabel background = new JLabel(new ImageIcon("images/bg.jpg"));
    private JTextField name = new JTextField(10);
    private JLabel nextBtn = new JLabel(new ImageIcon("images/nextBtn.png"));

    private JLabel title = new JLabel("  TRIVIA GAME");
    private JButton shortansBtn = new JButton(new ImageIcon("images/shortansBtn.png"));
    private JButton multipleBtn = new JButton(new ImageIcon("images/multipleBtn.png"));

    private JLabel questionNo = new JLabel();
    private JLabel jta = new JLabel();
    private JLabel stopBtn = new JLabel(new ImageIcon("images/stopBtn.png"));

    private JLabel gameModeText = new JLabel();

    private JButton sendBtn = new JButton(new ImageIcon("images/sendBtn.png"));
    private JLabel timerBg = new JLabel(new ImageIcon("images/timer.png"));
    private JTextField jtf = new JTextField();
    private JLabel scorelabel = new JLabel("0");

    private Font mainFont;
    private Color color = new Color(0, 153, 153);
    private Color highlighted = new Color(13, 81, 81);

    boolean soundOn = true;
    BooleanControl muteControl;

    private int currentChoice;

    private JButton a1 = new JButton("");
    private JButton a2 = new JButton("");
    private JButton a3 = new JButton("");
    private JButton a4 = new JButton("");
    private JLabel timelabel = new JLabel("30");

    private ArrayList<String> answerlist;
    private BufferedReader input;
    private PrintWriter output;
    private String mode;
    private String score;
    private Timer timer;
    private boolean timerIsRunning;

    public static void main(String[] args) throws FontFormatException, IOException {
        TriviaClient client = new TriviaClient();
    }

    public TriviaClient() throws FontFormatException, IOException {

        mainFont = Font.createFont(Font.TRUETYPE_FONT, new File("font/Bariol_Regular_0.otf"));

        /*first Screen*/
        welcome.setLayout(new BorderLayout(14, 21));
        JLabel welcomeMessage = new JLabel("YOUR NAME IS");
        welcomeMessage.setFont(mainFont.deriveFont(32.0F));
        welcomeMessage.setForeground(color);
        mainFont.deriveFont(16.5F);
        name.setFont(mainFont.deriveFont(18.0F));
        name.setForeground(Color.WHITE);
        name.setColumns(10);
        name.setHorizontalAlignment(JTextField.CENTER);
        name.setOpaque(false);
        name.setBorder(null);

        JLabel nameHolder = new JLabel(new ImageIcon("images/nameBg.png"));
        nameHolder.setPreferredSize(new Dimension(215, 34));
        nameHolder.setLayout(new BorderLayout());
        nameHolder.add(name);

        welcomeMessage.setForeground(color);
        welcome.add(welcomeMessage, BorderLayout.NORTH);
        welcome.add(nameHolder, BorderLayout.CENTER);
        welcome.add(nextBtn, BorderLayout.EAST);
        nextBtn.addMouseListener(new ButtonListener());
        welcome.setBorder(BorderFactory.createEmptyBorder(235, 282, 282, 309));
        welcome.setBackground(Color.BLACK);

        /*start screen*/
        start.setLayout(new GridLayout(3, 0, 0, 21));
        start.setBackground(Color.BLACK);

        title.setFont(mainFont.deriveFont(35.0F));
        title.setForeground(color);

        start.add(title);
        start.add(shortansBtn);
        start.add(multipleBtn);
        //shortans Button
        shortansBtn.setPreferredSize(new Dimension(216, 38));
        shortansBtn.setOpaque(false);
        shortansBtn.setContentAreaFilled(false);
        shortansBtn.setBorderPainted(false);
        shortansBtn.setBorder(null);
        shortansBtn.addMouseListener(new ButtonListener());

        //multiple choice Button
        multipleBtn.setPreferredSize(new Dimension(216, 38));
        multipleBtn.setOpaque(false);
        multipleBtn.setContentAreaFilled(false);
        multipleBtn.setBorderPainted(false);
        multipleBtn.setBorder(null);
        multipleBtn.addMouseListener(new ButtonListener());

        start.setBorder(BorderFactory.createEmptyBorder(230, 309, 230, 309));

        /*shortAnswer screen*/
        gameScreen.setLayout(null);
        gameScreen.setBackground(Color.BLACK);

        questionNo.setFont(mainFont.deriveFont(53.0F));
        questionNo.setForeground(Color.WHITE);
        questionNo.setOpaque(false);
        questionNo.setBorder(null);
        questionNo.setBounds(70, 210, 65, 50);

        jta.setFont(mainFont.deriveFont(21.0F));
        jta.setForeground(Color.WHITE);
        jta.setOpaque(false);
        jta.setBorder(null);
        jta.setBounds(70, 267, 371, 95);

        JLabel decoration = new JLabel(new ImageIcon("images/decoration.png"));
        decoration.setBounds(474, 140, 12, 238);

        gameModeText.setFont(mainFont.deriveFont(32.0F));
        gameModeText.setForeground(color);
        gameModeText.setBounds(125, 72, 215, 29);
        JLabel tute = new JLabel("Every right answer get 1 score");
        tute.setFont(mainFont.deriveFont(12.0F));
        tute.setForeground(new Color(153, 153, 153));
        tute.setBounds(161, 107, 156, 11);

        JLabel scoreBg = new JLabel(new ImageIcon("images/score.png"));
        scoreBg.setBounds(556, 71, 215, 213);

        scorelabel.setFont(mainFont.deriveFont(27.0F));
        scorelabel.setForeground(Color.BLACK);
        scorelabel.setBounds(655, 230, 35, 35);

        stopBtn.setBounds(626, 335, 113, 75);
        stopBtn.addMouseListener(new ButtonListener());

        JLabel decoration2 = new JLabel(new ImageIcon("images/decoration 2.png"));
        decoration2.setBounds(46, 462, 441, 14);

        jtf.setBorder(null);
        jtf.setOpaque(false);
        jtf.setColumns(30);
        jtf.setFont(mainFont.deriveFont(16.0F));
        jtf.setForeground(Color.BLACK);
        jtf.setBounds(55, 487, 370, 70);
        jtf.setBackground(color);
        jtf.setText("SDfgsdfgsdfgsdfg");

        sendBtn.setPreferredSize(new Dimension(84, 84));
        sendBtn.setOpaque(false);
        sendBtn.setContentAreaFilled(false);
        sendBtn.setBorderPainted(false);
        sendBtn.setBorder(null);
        sendBtn.addMouseListener(new ButtonListener());
        sendBtn.setBounds(444, 481, 84, 84);

        timerBg.setBounds(527, 504, 51, 41);

        timelabel.setFont(mainFont.deriveFont(24.0F));
        timelabel.setForeground(Color.BLACK);
        timelabel.setBounds(544, 513, 30, 27);

        gameScreen.add(gameModeText);
        gameScreen.add(tute);
        gameScreen.add(stopBtn);
        gameScreen.add(decoration);
        gameScreen.add(decoration2);
        gameScreen.add(jtf);
        gameScreen.add(sendBtn);

        gameScreen.add(questionNo);
        gameScreen.add(jta);

        gameScreen.add(timelabel);
        gameScreen.add(scorelabel);

        gameScreen.add(timerBg);
        gameScreen.add(scoreBg);

        /*multiple screen*/
        JPanel choices = new JPanel();   // panel for multiple choices
        choices.setLayout(new GridLayout(2, 2, 4, 4));
        choices.setPreferredSize(new Dimension(386, 87));

        a1.setFont(mainFont.deriveFont(16.0F));
        a1.setOpaque(false);
        //a1.setContentAreaFilled(false);
        //a1.setForeground(Color.WHITE);
        a1.setBackground(color);
        a1.setOpaque(false);
        a2.setFont(mainFont.deriveFont(16.0F));
        a2.setBackground(color);
        //a2.setForeground(Color.WHITE);
        a3.setFont(mainFont.deriveFont(16.0F));
        a3.setOpaque(false);
        a3.setBackground(color);
        //a3.setForeground(Color.WHITE);
        a4.setFont(mainFont.deriveFont(16.0F));
        a4.setOpaque(false);
        a4.setBackground(color);
        //a4.setForeground(Color.WHITE);   
        
        a1.setBorder(null);
        a2.setBorder(null);
        a3.setBorder(null);
        a4.setBorder(null);
        
        a1.addMouseListener(new ButtonListener());
        a2.addMouseListener(new ButtonListener());
        a3.addMouseListener(new ButtonListener());
        a4.addMouseListener(new ButtonListener());
        
        choices.add(a1);
        choices.add(a2);
        choices.add(a3);
        choices.add(a4);
        choices.setBounds(47, 490, 386, 87);
        choices.setOpaque(false);

        gameScreen.add(choices);

        layout = new CardLayout();
        all = new JPanel(layout);
        all.add(welcome, WELCOME);
        all.add(start, START);
        all.add(gameScreen, GAMESCREEN);

        layout.show(all, WELCOME);

        add(all);
        setSize(842, 595);
        getContentPane().setPreferredSize(new Dimension(842, 595));
        pack();
        setLocationRelativeTo(null);
        setTitle("Multiplayer Trivia Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);

        //sound and quit button
        for (int i = 0; i < helloNames.length; i++) {
            helloNames[i] = new HelloName();
        }

        for (int i = 0; i < soundBtns.length; i++) {
            soundBtns[i] = new SoundBtn();
        }

        welcome.setLayout(null);
        welcome.add(soundBtns[0]);
        welcome.add(new QuitBtn());

        start.setLayout(null);
        start.add(new BackBtn());
        start.add(soundBtns[1]);
        start.add(new QuitBtn());
        start.add(helloNames[0]);

        gameScreen.setLayout(null);
        gameScreen.add(new BackBtn());
        gameScreen.add(soundBtns[2]);
        gameScreen.add(new QuitBtn());
        gameScreen.add(helloNames[1]);

        /*sound*/
        try {
            // Open an audio input stream.
            File bgMusic = new File("sound/bg.wav");
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(bgMusic);
            // Get a sound clip resource.
            Clip clip = AudioSystem.getClip();
            // Open audio clip and load samples from the audio input stream.
            clip.open(audioIn);
            muteControl
                    = (BooleanControl) clip.getControl(BooleanControl.Type.MUTE);
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
        try {
            Socket socket = new Socket("localhost", 8000);
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true); // set true for autoflush
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    public void timerRestrict() {   // countdown clock for answering time
        timerIsRunning = true;
        timelabel.setText("30");
        timer = new Timer(1000, new TimerListener());
        timer.start();
    }

    public void displayMPC() {  // display question and answer choices for multiple choice mode
        try {
            gameModeText.setText("Multiple Choices");
            String answer1, answer2, answer3, answer4;
            a1.setEnabled(true);
            a2.setEnabled(true);
            a3.setEnabled(true);
            a4.setEnabled(true);
            a1.setVisible(true);
            a2.setVisible(true);
            a3.setVisible(true);
            a4.setVisible(true);

            jtf.setVisible(false);
            jtf.setEnabled(false);
            
            String question = input.readLine();
            jta.setText("<html><p>" + question + "</p></html>"); // initialize question from server                 
            
            if(question.equals("End")) {
                questionNo.setText("0");
                JOptionPane.showMessageDialog(null, 
                        "You have finished all the questions", "Finish",
                        JOptionPane.PLAIN_MESSAGE);
                changeState(START);
            } 
            else {
                jtf.setText("");
                timerRestrict();
                answer1 = input.readLine(); // receive multiple choices
                answer2 = input.readLine();
                answer3 = input.readLine();
                answer4 = input.readLine();          

                answerlist = new ArrayList<>(Arrays.asList(new String[]{answer1, answer2, answer3, answer4}));
                Collections.shuffle(answerlist); // randomize answers' orders
                a1.setText(answerlist.get(0));
                a2.setText(answerlist.get(1));
                a3.setText(answerlist.get(2));
                a4.setText(answerlist.get(3));
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    public void displayNormal() {    // display question for normal trivia mode
        try {
            gameModeText.setText("   Short Answers");
            jtf.setVisible(true);
            jtf.setEnabled(true);
            jtf.setOpaque(true);

            a1.setEnabled(false);
            a2.setEnabled(false);
            a3.setEnabled(false);
            a4.setEnabled(false);
            a1.setVisible(false);
            a2.setVisible(false);
            a3.setVisible(false);
            a4.setVisible(false);
            
            String question = input.readLine();
            jta.setText("<html><p>" + question + "</p></html>"); // initialize question from server
            if(question.equals("End")) {
                questionNo.setText("0");
                JOptionPane.showMessageDialog(null, 
                        "You have finished all the questions", "Finish",
                        JOptionPane.PLAIN_MESSAGE);
                changeState(START);
            } 
            else {
                jtf.setText("");
                timerRestrict();
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    public void getResult() {
        try {

            String result = input.readLine(); // receive result
            score = input.readLine();

            if (result.equals("Right")) {     // compare the answer
                if (soundOn) {
                    try {
                        // Open an audio input stream.
                        File bgMusic = new File("sound/right.wav");
                        AudioInputStream audioIn
                                = AudioSystem.getAudioInputStream(bgMusic);
                        // Get a sound clip resource.
                        Clip clip = AudioSystem.getClip();
                        // Open audio clip and load samples from the audio input stream.
                        clip.open(audioIn);
                        clip.start();
                    } catch (UnsupportedAudioFileException | IOException | LineUnavailableException f) {
                        f.printStackTrace();
                    }
                }
            } else {
                if (soundOn) {
                    try {
                        // Open an audio input stream.
                        File bgMusic = new File("sound/wrong.wav");
                        AudioInputStream audioIn
                                = AudioSystem.getAudioInputStream(bgMusic);
                        // Get a sound clip resource.
                        Clip clip = AudioSystem.getClip();
                        // Open audio clip and load samples from the audio input stream.
                        clip.open(audioIn);
                        clip.start();
                    } catch (UnsupportedAudioFileException | IOException | LineUnavailableException f) {
                        f.printStackTrace();
                    }
                }
            }

            scorelabel.setText(score); // Display score
            if (mode.equals("Normal")) {
                displayNormal();
            } else if (mode.equals("MPC")) {
                displayMPC();
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    private class EnterListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) { // answer for normal mode                 
            String answer = jtf.getText();

            output.println(answer);   // send the answer from server

            timer.stop();
            timerIsRunning = false;
            getResult();
        }
    }

    private class ButtonListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getSource() == shortansBtn) { // play in normal mode
                questionNo.setText("1");
                changeState(GAMESCREEN);
                mode = "Normal";
                output.println(mode);

                if (timerIsRunning) {
                    timer.stop();
                    timerIsRunning = false;
                }

                jtf.requestFocus();
                displayNormal();
            } else if (e.getSource() == multipleBtn) {   // play in multiple choice mode
                questionNo.setText("1");
                changeState(GAMESCREEN);
                mode = "MPC";
                output.println(mode);

                if (timerIsRunning) {
                    timer.stop();
                    timerIsRunning = false;
                }
                displayMPC();
            } else if (e.getSource() == sendBtn) {
                questionNo.setText((Integer.parseInt(questionNo.getText()) + 1) + "");
                if (mode == "Normal") {
                    String answer = jtf.getText();

                    output.println(answer);   // send the answer from server

                    timer.stop();
                    timerIsRunning = false;
                    getResult();
                } else {
                    a1.setBackground(color);
                    a2.setBackground(color);
                    a3.setBackground(color);
                    a4.setBackground(color);
                    output.println(answerlist.get(currentChoice));                                  
                    timer.stop();
                    timerIsRunning = false;
                    getResult();
                }

            } else if (e.getSource() == a1) {
                currentChoice = 0;
                a2.setBackground(color);
                a3.setBackground(color);
                a4.setBackground(color);
                a1.setBackground(highlighted);
            } else if (e.getSource() == a2) {
                currentChoice = 1;
                a1.setBackground(color);
                a3.setBackground(color);
                a4.setBackground(color);
                a2.setBackground(highlighted);
            } else if (e.getSource() == a3) {
                currentChoice = 2;
                a2.setBackground(color);
                a1.setBackground(color);
                a4.setBackground(color);
                a3.setBackground(highlighted);
            } else if (e.getSource() == a4) {
                currentChoice = 3;
                a2.setBackground(color);
                a3.setBackground(color);
                a1.setBackground(color);
                a4.setBackground(highlighted);
            } else if (e.getSource() == nextBtn) {
                changeState(START);
            } else if (e.getSource() instanceof BackBtn) {
                layout.previous(all);
                for (SoundBtn s : soundBtns) {
                    s.still();
                }
                for (HelloName s : helloNames) {
                    s.changeText();
                }
            } else if (e.getSource() instanceof SoundBtn) {
                ((SoundBtn) e.getSource()).clicked();
                muteControl.setValue(!soundOn);
            } else if (e.getSource() instanceof QuitBtn) {
                System.exit(0);
            } else if (e.getSource() == stopBtn) {
                changeState(START);
                if(timerIsRunning) {
                    timer.stop();
                }                
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            if (e.getSource() == shortansBtn) {
                shortansBtn.setIcon(new ImageIcon("images/shortansBtnHover.png"));
            } else if (e.getSource() == multipleBtn) {
                multipleBtn.setIcon(new ImageIcon("images/multipleBtnHover.png"));
            } else if (e.getSource() == nextBtn) {
                nextBtn.setIcon(new ImageIcon("images/nextBtnHover.gif"));
            } else if (e.getSource() instanceof BackBtn) {
                ((BackBtn) e.getSource()).setIcon(new ImageIcon("images/backBtnHover.gif"));
            } else if (e.getSource() instanceof SoundBtn) {
                ((SoundBtn) e.getSource()).hover();
            } else if (e.getSource() instanceof QuitBtn) {
                ((QuitBtn) e.getSource()).setForeground(Color.WHITE);
            } else if (e.getSource() == stopBtn) {
                stopBtn.setIcon(new ImageIcon("images/stopBtnHover.gif"));
            } else if (e.getSource() == sendBtn) {
                sendBtn.setIcon(new ImageIcon("images/sendBtnHover.png"));
            }

            if (soundOn) {
                try {
                    // Open an audio input stream.
                    File bgMusic = new File("sound/mouseOver.wav");
                    AudioInputStream audioIn
                            = AudioSystem.getAudioInputStream(bgMusic);
                    // Get a sound clip resource.
                    Clip clip = AudioSystem.getClip();
                    // Open audio clip and load samples from the audio input stream.
                    clip.open(audioIn);
                    clip.start();
                } catch (UnsupportedAudioFileException | IOException | LineUnavailableException f) {
                    f.printStackTrace();
                }
            }

        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        }

        @Override
        public void mouseExited(MouseEvent e) {
            if (e.getSource() == shortansBtn) {
                shortansBtn.setIcon(new ImageIcon("images/shortansBtn.png"));
            } else if (e.getSource() == multipleBtn) {
                multipleBtn.setIcon(new ImageIcon("images/multipleBtn.png"));
            } else if (e.getSource() == nextBtn) {
                nextBtn.setIcon(new ImageIcon("images/nextBtn.png"));
            } else if (e.getSource() instanceof BackBtn) {
                ((BackBtn) e.getSource()).setIcon(new ImageIcon("images/backBtn.png"));
            } else if (e.getSource() instanceof SoundBtn) {
                ((SoundBtn) e.getSource()).still();
            } else if (e.getSource() instanceof QuitBtn) {
                ((QuitBtn) e.getSource()).setForeground(color);
            } else if (e.getSource() == stopBtn) {
                stopBtn.setIcon(new ImageIcon("images/stopBtn.png"));
            } else if (e.getSource() == sendBtn) {
                sendBtn.setIcon(new ImageIcon("images/sendBtn.png"));
            }
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void changeState(String state) {
        layout.show(all, state);
        for (SoundBtn s : soundBtns) {
            s.still();
        }
        for (HelloName s : helloNames) {
            s.changeText();
        }
    }

    private class TimerListener implements ActionListener {

        private int interval = 30;  // set answering time is 30 seconds

        @Override
        public void actionPerformed(ActionEvent e) {
            interval--;
            if(interval<10) {
                timelabel.setText("0" + interval);
            }
            else {
                timelabel.setText("" + interval);
            }
            if (interval == 0) {
                timer.stop();
                timerIsRunning = false;
                output.println("");     // run out of time, automatically send wrong answer
                getResult();
            }
        }

    }

    private class SoundBtn extends JLabel {

        public SoundBtn() {
            setBounds(793, 0, 33, 42);
            setIcon(new ImageIcon("images/soundBtn.png"));
            addMouseListener(new ButtonListener());
        }

        public void clicked() {
            soundOn = !soundOn;
            still();
        }

        public void still() {
            if (soundOn) {
                setIcon(new ImageIcon("images/soundBtn.png"));
            } else {
                setIcon(new ImageIcon("images/soundOffBtn.png"));
            }
        }

        public void hover() {
            if (soundOn) {
                setIcon(new ImageIcon("images/soundOnHover.gif"));
            } else {
                setIcon(new ImageIcon("images/soundOffHover.gif"));
            }
        }
    }

    private class BackBtn extends JLabel {

        public BackBtn() {
            setBounds(27, 13, 28, 18);
            setIcon(new ImageIcon("images/backBtn.png"));
            addMouseListener(new ButtonListener());
        }
    }

    private class QuitBtn extends JLabel {

        public QuitBtn() {
            setText("QUIT");
            setBounds(772, 550, 55, 24);
            setFont(mainFont.deriveFont(22.0F));
            setForeground(color);
            addMouseListener(new ButtonListener());
        }
    }

    private class HelloName extends JLabel {

        public HelloName() {
            setBounds(361, 16, 150, 20);
            setFont(mainFont.deriveFont(18.0F));
            setForeground(color);
        }

        public void changeText() {
            if (name.getText().equals("")) {
                setText("Hi, Little Friend");
            } else {
                setText("Hi, " + name.getText());
            }
        }
    }

}
