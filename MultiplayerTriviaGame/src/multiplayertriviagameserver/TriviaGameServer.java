package multiplayertriviagameserver;

import java.awt.BorderLayout;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class TriviaGameServer extends JFrame {
    
    private JTextArea jta = new JTextArea();
    private int clientNo; 
    private ArrayList<Trivia> list;
    
    public static void main(String[] args) {
        new TriviaGameServer();
    }
    
    public TriviaGameServer() {
        setLayout(new BorderLayout());
        add(new JScrollPane(jta), BorderLayout.CENTER);
        setSize(500,300);
        setVisible(true);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
        // Load DB drivers
        try {
            Class.forName("org.postgresql.Driver");
        }
        catch (ClassNotFoundException ex){
            System.err.println(ex);
        }
        
        // Establish connections
        Connection connection = null;
        try {
            connection = DriverManager.getConnection
              ("jdbc:postgresql://localhost:5432/postgres", "postgres", "1234");
           
            // SQL statements
            Statement statement = connection.createStatement();
            statement.executeUpdate("set search_path = trivia");
            ResultSet question_set = statement.executeQuery("select question, "
                  + "right_answer, answer_2, answer_3, answer_4 from question");
            
            // Create an arraylist to store questions and answers
            list = new ArrayList<>(50);
            while(question_set.next()) {
                list.add(new Trivia(question_set.getString(1)
                       , question_set.getString(2), question_set.getString(3)
                       , question_set.getString(4), question_set.getString(5)));
            }           
            
        } catch (SQLException ex) {
            System.err.println(ex);
        }
        
        // Create Server socket to accept communication from clients
        try {
            ServerSocket serverSocket = new ServerSocket(8000);
            jta.append("Trivia Server started at "+new Date()+'\n');
            
            clientNo = 1;
            
            while (true) {
                Socket socket = serverSocket.accept(); // listen to new connection request
                jta.append("Starting Thread for Client " + clientNo + " at "
                + new Date() + "\n");              
                
                ClientHandler task = new ClientHandler(socket);
                Thread t = new Thread(task);
                
                t.start();
                clientNo++;
            }           
        }
        catch(IOException ex) {
            System.err.println("Accept failed.");
            System.exit(1);
        }
    }
    
    class ClientHandler implements Runnable {
        private Socket socket;
        private int score;       
        private BufferedReader input;
        private PrintWriter output;
        private String mode; 
        private ArrayList<Trivia> qalist = new ArrayList<>(50);
        private boolean threadRunning = true;  // a boolean flag to termninate thread
        
        public ClientHandler(Socket socket) {
            this.socket = socket;
            for(Trivia t: list) {       // copy a new arraylist of questions and answers
                qalist.add(new Trivia(t));
            }
        }
        
        public void playGame() {    // start the game with client
            try {
                if(mode.equals("Normal")) { // Normal Mode 
                    score = 0;
                    Collections.shuffle(qalist);    // each time play has a random set of questions 
                    ListIterator<Trivia> i = qalist.listIterator();
                    while(i.hasNext()) {
                        try {
                            Trivia questionAnswer = i.next();
                            output.println(questionAnswer.getQuestion()); // send question to client
                            String clientAnswer = input.readLine(); // receive answer from client  
                            if(clientAnswer!=null && clientAnswer.trim().
                                    equalsIgnoreCase(questionAnswer.getAnswer())) { // check the answer
                                score++;
                                output.println("Right");
                            }
                            else if(clientAnswer.equals("Normal")) {    // for client wanting to change mode
                                mode = "Normal";
                                playGame();
                            }
                            else if(clientAnswer.equals("MPC")) {    // for client wanting to change mode
                                mode = "MPC";
                                playGame();
                            }
                            else {
                                output.println(questionAnswer.getAnswer()); // if answer is wrong send the right answer
                            }
                            output.println(Integer.toString(score)); // update client's score                          
                        } catch (IOException ex) {
                            System.err.println(ex);
                        }
                    }
                    output.println("End");  // all questions completed
                    try {
                        mode = input.readLine();
                        playGame();
                    } catch (IOException ex) {
                        System.err.println(ex);
                    }                   
                }
                else if(mode.equals("MPC")) {   // Multiple Choice Mode
                    score = 0;
                    ArrayList<Trivia> qalist = list;
                    Collections.shuffle(qalist);
                    ListIterator<Trivia> i = qalist.listIterator();
                    while(i.hasNext()) {
                        try {
                            Trivia questionAnswer = i.next();
                            output.println(questionAnswer.getQuestion()); // send question and multiple choices to client
                            output.println(questionAnswer.answer);
                            output.println(questionAnswer.wrong_answer1);
                            output.println(questionAnswer.wrong_answer2);
                            output.println(questionAnswer.wrong_answer3);
                            String clientAnswer = input.readLine();     // receive answer
                            if(clientAnswer.equals(questionAnswer.getAnswer())) {
                                score ++;
                                output.println("Right");
                            }
                            else if(clientAnswer.equals("Normal")) {    // for client wanting to change mode
                                mode = "Normal";
                                playGame();
                            }
                            else if(clientAnswer.equals("MPC")) {    // for client wanting to change mode
                                mode = "MPC";
                                playGame();
                            }
                            else {
                                output.println(questionAnswer.getAnswer());
                            }
                            output.println(Integer.toString(score));
                        } catch (IOException ex) {
                            System.err.println(ex);
                        }
                    }
                    output.println("End");  // all questions completed
                    try {
                        mode = input.readLine();
                        playGame();
                    } catch (IOException ex) {
                        System.err.println(ex);
                    }
                }  
            }
            catch (NullPointerException e) {   // null pointer exception means client have closed connection
                try {
                    socket.close();
                    threadRunning = false;
                } catch (IOException ex) {
                    System.err.println(ex);
                }              
            }
        }
        
        @Override
        public void run() {
            try {
                input = new BufferedReader
                            (new InputStreamReader(socket.getInputStream()));
                output = new PrintWriter(new BufferedWriter
                    (new OutputStreamWriter(socket.getOutputStream())),true); // set true for autoflush
                mode = input.readLine(); // read the mode choice from client
                while(threadRunning) {
                    playGame();
                }             
            }
            catch(IOException ex) {
                System.err.println(ex);
            }
        }          
    }   
    
    class Trivia { // Object stored questions and answers
        private String question;
        private String answer;
        private String wrong_answer1;
        private String wrong_answer2;
        private String wrong_answer3; 
        
        public Trivia(String question, String answer,
            String wrong_answer1, String wrong_answer2, String wrong_answer3) {
            this.question = question;
            this.answer = answer;
            this.wrong_answer1 = wrong_answer1;
            this.wrong_answer2 = wrong_answer2;
            this.wrong_answer3 = wrong_answer3;
        }
        
        public Trivia(Trivia trivia) {
            this.question = trivia.getQuestion();
            this.answer = trivia.getAnswer();
            this.wrong_answer1 = trivia.getWrong_answer1();
            this.wrong_answer2 = trivia.getWrong_answer2();
            this.wrong_answer3 = trivia.getWrong_answer3();
        }
        
        public String getQuestion() {
            return question;
        }

        public String getAnswer() {
            return answer;
        }  
        
        public String getWrong_answer1() {
            return wrong_answer1;
        }

        public String getWrong_answer2() {
            return wrong_answer2;
        }

        public String getWrong_answer3() {
            return wrong_answer3;
        }
    }
   
}
