The Trivia Game is a simple game that requires clients to connect to a server and retrieve questions to answer. The server then evaluates answers and returns results to the clients. A correct answer receives one score.

The server allows multiple clients to take place at any one time which includes (A console based of Trivia Game)

**Server Program**
* Using appropriate protocol to interact with clients o Load and generate questions to clients.

* Initiating a separated thread for each client.
* Get the answers from clients and evaluate
* Return results to clients
* No question is asked twice
* Questions and answers saved in database (see instructions in Database Guidelines)

**Client Program**

* Using appropriate protocol to get connected with server o Display questions from the server
* Implemented UI with Java Swing
* Display results from server
* The server enforce a time limit on a player answering a question. 


###Language: 
Java, Java Swing

###Contributors:

* Ha Nguyen
* Huy Tran Tan