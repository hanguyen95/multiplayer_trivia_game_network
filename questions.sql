﻿CREATE SCHEMA trivia;
SET search_path TO trivia;

CREATE TABLE question(
	question TEXT PRIMARY KEY,
	right_answer TEXT,
	answer_2 TEXT,
	answer_3 TEXT,
	answer_4 TEXT);

INSERT INTO question VALUES('Who invented the BALLPOINT PEN', 'Biro Brothers', 'Waterman Brothers', 'Bicc Brothers', 'Write Brothers');
INSERT INTO question VALUES('Entomology is the science that studies...', 'Insects', 'Behavior of human beings','Language','The formation of rocks');
INSERT INTO question VALUES('Hitler party which came into power in 1933 is known as...', 'Nazi Party', 'Labour Party', 'Ku-Klux-Klan', 'Democratic Party');
INSERT INTO question VALUES('The book ''To Live or Not Live'' was written by...', 'Nirad C. Chaudhuri', 'V.S. Naipaul', 'Alexandra Dumas',  'George Elliot');
INSERT INTO question VALUES('In India the first television programme was broadcasted in...', '1959','1965','1976','1957');
INSERT INTO question VALUES('Which scientist discovered the radioactive element radium?', 'Marie Curie', 'Isaac Newton','Albert Einstein','Benjamin Franklin');
INSERT INTO question VALUES('Nuclear sizes are expressed in a unit named...', 'fermi', 'angstrom', 'newton', 'tesla');
INSERT INTO question VALUES('Light year is a unit of...', 'distance', 'light', 'intensity of light');
INSERT INTO question VALUES('Light from the Sun reaches us in nearly...', '8 minutes', '2 minutes', '4 minutes', '16 minutes');
INSERT INTO question VALUES('Pa(Pascal) is the unit for...', 'pressure', 'thrust', 'frequency', 'conductivity');
INSERT INTO question VALUES('''A Voice for Freedom'' is a book written by', 'Nayantara Sahgal', 'Corazon Aquino', 'Aung San Suu Kyi', 'Benazir Bhutto');
INSERT INTO question VALUES('Who is the father of English Poetry?', 'Chaucer', 'Milton', 'Wordsworth', 'Charles Dickens');
INSERT INTO question VALUES('Who wrote ''War and Peace''?', 'Leo Tolstoy', 'Mahatma Gandhi', 'Charles Dickens', 'Kipling');
INSERT INTO question VALUES('First human heart transplant operation conducted by Dr. Christian Bernard on Louis Washkansky, was conducted in...', '1967', '1968', '1958', '1922');
INSERT INTO question VALUES('Golf player Vijay Singh belongs to which country?', 'Fiji', 'USA', 'India', 'UK');
INSERT INTO question VALUES('First Afghan War took place in...', '1839', '1843', '1833', '1848');
INSERT INTO question VALUES('For the Olympics and World Tournaments, the dimensions of basketball court are...', '28 m x 15 m', '26 m x 14 m', '27 m x 16 m', '28 m x 16 m');
INSERT INTO question VALUES('Each year World Red Cross and Red Crescent Day is celebrated on...', 'May 8', 'May 18', 'June 8', 'June 18');
INSERT INTO question VALUES('What is part of a database that holds only one type of information?', 'Field', 'Report', 'Record', 'File');
INSERT INTO question VALUES('''OS'' computer abbreviation usually means ?', 'Operating System', 'Order of Significance', 'Open Software', 'Optical Sensor');
INSERT INTO question VALUES('''.MOV'' extension refers usually to what kind of file?', 'Movie file', 'Image file', 'Audio file', 'MS Office document');
INSERT INTO question VALUES('Who has been awarded the first lifetime Achievement Award for his/her contribution in the field of Cinema?', 'Ashok Kumar', 'Hou Hsio-hsein', 'Akiro Burosova', 'Bernardo Burtolucci');
INSERT INTO question VALUES('Who is the first Asian Winner of Nobel Prize?', 'Rabindranath Tagore', 'C. V. Raman', 'Rajiv Gandhi', 'Mother Teresa');
INSERT INTO question VALUES('Who among the following has received a Nobel Prize in literature 1953?', 'Winston Churchill', 'Pablo Neruda', 'Derek Walcott', 'Ernest Hemingway');
INSERT INTO question VALUES('During World War II, when did Germany attack France?', '1940', '1941', '1942', '1943');
INSERT INTO question VALUES('Who is the father of Geometry?', 'Euclid', 'Aristotle', 'Pythagoras', 'Kepler');
INSERT INTO question VALUES('The nucleus of an atom consists of...', 'protons and neutrons', 'electrons and neutrons', 'electrons and protons', 'All of the above');
INSERT INTO question VALUES('The metal used to recover copper from a solution of copper sulphate is...', 'Fe', 'Na', 'Ag', 'Hg');
INSERT INTO question VALUES('Sound waves in air are...', 'longitudinal', 'transverse', 'electromagnetic', 'polarised');
INSERT INTO question VALUES('What Galileo invented?', 'Thermometer', 'Barometer', 'Pendulum clock', 'Microscope');
INSERT INTO question VALUES('Who invented Jet Engine?', 'Sir Frank Whittle', 'Gottlieb Daimler', 'Roger Bacon', 'Lewis E. Waterman');
INSERT INTO question VALUES('What invention caused many deaths while testing it?', 'Parachute', 'Dynamite', 'Ladders', 'Race cars');
INSERT INTO question VALUES('Who invented Gunpowder?', 'Roger Bacon', 'G. Ferdinand Von Zeppelin', 'Sir Frank Whittle', 'Leo H Baekeland');
INSERT INTO question VALUES('In which decade was the telephone invented?', '1870s', '1850s', '1860s', '1880s');
INSERT INTO question VALUES('What Benjamin Franklin invented?', 'Bifocal spectacles', 'Radio', 'Barometer', 'Hygrometer');
INSERT INTO question VALUES('When was Monopoly created?', '1930s', '1940s', '1920s', '1950s');
INSERT INTO question VALUES('Sound of frequency below 20 Hz is called...', 'infrasonic', 'audio sounds', 'ultrasonic', 'supersonics');
INSERT INTO question VALUES('Sound travels at the fastest speed in...', 'steel', 'water', 'air', 'vacuum');
INSERT INTO question VALUES('Light travels at the fastest speed in', 'vacuum', 'glass', 'water', 'hydrogen');
INSERT INTO question VALUES('What furniture item was invented by California furniture designer Charles Prior Hall in 1968?', 'Waterbed', 'Sofa bed', 'Captain''s chair', 'Hammock');
INSERT INTO question VALUES('Who is the English physicist responsible for the ''Big Bang Theory''?', 'George Gamow', 'Albert Einstein', 'Michael Skube', 'Roger Penrose');
INSERT INTO question VALUES('When was the first elevator built?', '1743', '1739', '1760', '1785');
INSERT INTO question VALUES('Who invented the small pox vaccine?', 'Edward Jenner', 'Robert Koch', 'Robert Hooke', 'Louis Pasteur');
INSERT INTO question VALUES('Where was the yo-yo invented?', 'Philippines', 'France', 'United States', 'England');
INSERT INTO question VALUES('The first hand glider was designed by...', 'Leonardo DaVinci', 'The Wright brothers', 'Francis Rogallo', 'Galileo');
INSERT INTO question VALUES('''.MPG'' extension refers usually to what kind of file?', 'Movie file', 'WordPerfect Document file', 'MS Office document', 'Image file');
INSERT INTO question VALUES('''DB'' computer abbreviation usually means ?', 'Database', 'Double Byte', 'Data Block', 'Driver Boot');
INSERT INTO question VALUES('The sampling rate for a CD is...?', '44.1 kHz', '48.4 kHz', '22,050 Hz', '48 kHz');
INSERT INTO question VALUES('When were bar code scanners invented?', '1970s', '1940s', '1950s', '1960s');
INSERT INTO question VALUES('When was the game Frisbee invented?', '1870s', '1920s', '1900s', '1890s');


